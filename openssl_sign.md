# OenSSL command: [Ref](https://www.sslshopper.com/article-most-common-openssl-commands.html)

## Create key
- Private key:
    ```
    openssl genrsa -out privateKey.key 2048
    ```
- Create public key from private key:
    ```
    openssl rsa -in rsa.private -out rsa.public -pubout -outform PEM
    ```

## Generate a certificate signing request (CSR) for an existing private key
-
    ```
    openssl req -new -out CSR.csr -key privateKey.key -subj "/CN=jean/O=$group1/O=$group2/O=$group3"
    ```

### Generate a certificate signing request based on an existing certificate
-
    ```
    openssl x509 -x509toreq -in certificate.crt -out CSR.csr -signkey privateKey.key
    ```

### Generate a self-signed certificate
-
    ```
    openssl req -x509 -sha256 -nodes -days 365 -newkey rsa:2048 -keyout privateKey.key -out certificate.crt
    ```


## Sign a CSR with CA.key / Output: CRT
-
    ```
    openssl x509 -req -in CSR.csr -CA /etc/kubernetes/pki/ca.crt -CAkey /etc/kubernetes/pki/ca.key -CAcreateserial -out CRT.crt -days 1000
    ```

## Check CRT
-
    ```
    openssl x509 -in certificate.crt -text -noout
    ```